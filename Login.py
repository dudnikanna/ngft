from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


driver = webdriver.Chrome()
driver.get("http://redmine.cleveroad.com:4016/admin/login")

email = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, "email")))
password = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, "password")))
login_button = driver.find_element_by_xpath("//button[@class='btn btn-primary block full-width m-b']")

email.send_keys("john.doe@ngft.com")
password.send_keys("123123123")
login_button.click()

#scroll to center of table
table = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, "tbody")))
center = table.find_elements(By.TAG_NAME, "tr")[12]
driver.execute_script("return arguments[0].scrollIntoView();", center)


